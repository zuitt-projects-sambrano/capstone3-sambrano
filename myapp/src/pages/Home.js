import { Fragment } from 'react';
import Banner from '../components/Banner';
import Highlights from '../components/Highlights'; 

export default function Home(){

	const data = {

		title: "D&J Volleyball Service",
		content: "Opportunities to learn volleyball!",
		destination: "/services",
		label: "Enroll Now!"
	}

	return(
		<Fragment>
			<Banner data={data}/>
			<Highlights/>
			{/*<CourseCard/>*/}
		</Fragment>
	)
}