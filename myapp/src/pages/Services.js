import React, {useState, useEffect, useContext} from 'react';

import {Container} from 'react-bootstrap'

import AdminView from './../components/AdminView.js';
import UserView from './../components/UserView.js';

import UserContext from './../UserContext';

export default function Services(){

	const [services, setServices] = useState([]);

	const {user} = useContext(UserContext);

	const fetchData = () => {

		fetch('https://dandj-volleyball.herokuapp.com/services/all')
		.then(result => result.json())
		.then(data => {
			console.log(data)
			setServices(data)
		})
	}

	useEffect(() => {
		fetchData()
	}, [])
 
	return(
		<Container className="p-4">
			{ (user.isAdmin === true) ?
					<AdminView serviceData={services} fetchData={fetchData}/>
				:
					<UserView serviceData={services} />
			}
		</Container>
	)
}