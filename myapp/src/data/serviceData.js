const servicesData = [
	{
		id: "wdc001",
		name: "Libero ",
		description: "Liberos wear a jersey of a different color and play in the back row five out the six rotations, usually subbing in for both middle blockers. When the libero comes in for another player, it does not count as a substitution. Liberos are defensive and serve-receive specialists who are typically fast and are able to change direction quickly. Liberos are not permitted to attack the ball from above the height of the net, and they can only overhand set a front-row attacker from behind the 10-foot line.",
		price: 3000,
		onOffer: true
	},
	{
		id: "wdc002",
		name: "Middle Blocker",
		description: "Middle blockers are the team’s best blockers, and they hit mostly fast-tempo sets from the middle of the court and behind the setter. Typically the libero goes in for the middle when he or she rotates to the back row. Middle sets are some of the most difficult to set and require good passes, therefore middles often get the fewest sets but have the best hitting percentages.",
		price: 3500,
		onOffer: true
	},
	{
		id: "wdc003",
		name: "Outside Hitter",
		description: "An outside hitter hits and blocks from the left side of the court. Normally, they also carry the responsibilities of passing and playing defense when they get to the back row. The outside typically gets the most sets, especially when the setter is out of system, due to the fact that the outside set is one of the easier options to set. An outside’s responsibilities include hitting from the front and back row, passing in serve receive, playing left or middle-back defense, and blocking.",
		price: 4000,
		onOffer: true
	},

];

export default servicesData;
