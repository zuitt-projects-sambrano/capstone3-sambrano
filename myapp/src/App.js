import { useState, useEffect } from 'react';
import { Container } from 'react-bootstrap';
import { BrowserRouter as Router, Route, Routes} from 'react-router-dom';
import AppNavBar from './components/AppNavBar';
import ServiceView from './components/ServiceView.js';
import Home from './pages/Home';
import Services from './pages/Services';
import Error from './pages/Error';
import Login from './pages/Login';
import Logout from './pages/Logout';
import Register from './pages/Register';
import './App.css';
import { UserProvider } from './UserContext'

function App() {

  const [user, setUser] = useState({
    id: null,
    isAdmin: null
  })


  const unsetUser = () => {
    localStorage.clear();
  }

  useEffect(() => {
    fetch('https://dandj-volleyball.herokuapp.com/users/details', {
      method: "POST",
      headers: {
        Authorization: `Bearer ${localStorage.getItem("token")}`
      }
    })
    .then(res => res.json())
    .then(data => {
      console.log(data)

      if(typeof data._id !== "undefined"){

        setUser({
          id: data._id,
          isAdmin: data.isAdmin
        })

      } else {
        setUser({
          id: null,
          isAdmin: null
        })
      }
    })

  }, [])


  return (
    <UserProvider value={{user, setUser, unsetUser}}>
    <Router>
      <AppNavBar/>
      <Container>
      <Routes>
          <Route exact path= "/" element={<Home/>}/>
          <Route exact path= "/services" element={<Services/>}/>
          <Route exact path= "/services/:serviceId" element={<ServiceView/>}/>
          <Route exact path= "/register" element={<Register/>}/>
          <Route exact path= "/login" element={<Login/>}/>
          <Route exact path= "/logout" element={<Logout/>}/>
          <Route exact path= "*" element={<Error/>}/>
      </Routes>
      </Container>
    </Router>
    </UserProvider>
  )
}

export default App;