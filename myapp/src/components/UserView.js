import { useEffect, useState } from "react"
import { Container } from "react-bootstrap"

import ServiceCard from './ServiceCard'

export default function UserView({serviceData}){
    console.log(serviceData)
    console.log(typeof serviceData)

    const [service, setServices] = useState([])

    useEffect(() => {
        
        const servicesArr = serviceData.map((service) => {
            console.log("am here")
            if (service.isActive === true){
                return <ServiceCard key={service._id} serviceProp={service}/>
                console.log("am here too")
            } else {
                return null
            }
        })

        setServices(servicesArr)

    }, [serviceData])

    return (
        <Container>
            {service}
        </Container>
    )
}