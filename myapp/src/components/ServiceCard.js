import {useState, useEffect} from 'react';
import {Card, Button} from 'react-bootstrap';
import {Link} from 'react-router-dom';

export default function ServiceCard({serviceProp}) {

    console.log(serviceProp);

    console.log(typeof serviceProp);
    
    const {name, description, price, _id} = serviceProp

    return (
        <Card>
            <Card.Body>
                <Card.Title>{name}</Card.Title>
                <Card.Subtitle>Description:</Card.Subtitle>
                <Card.Text>{description}</Card.Text>
                <Card.Subtitle>Price:</Card.Subtitle>
                <Card.Text>{price}</Card.Text>
                <Button variant="primary" as= {Link} to={`/services/${_id}`}>See Details</Button>
            </Card.Body>
        </Card>
    )
}
