import { Fragment, useState, useEffect } from 'react';
import { Table, Button, Modal, Form } from 'react-bootstrap';
import Swal from 'sweetalert2';

export default function AdminView(props){

	const { serviceData, fetchData } = props;

	const [serviceId, setServiceId] = useState("");
	const [services, setServices] = useState([]);
	const [name, setName] = useState("");
	const [description, setDescription] = useState("");
	const [price, setPrice] = useState(0);

	const [showEdit, setShowEdit] = useState(false);
	const [showAdd, setShowAdd] = useState(false);

	const openAdd = () => setShowAdd(true);
	const closeAdd = () => setShowAdd(false);

	const openEdit = (serviceId) => {

		fetch(`https://dandj-volleyball.herokuapp.com/services/${ serviceId }`)
		.then(res => res.json())
		.then(data => {

			setServiceId(data._id);
			setName(data.name);
			setDescription(data.description);
			setPrice(data.price);
		})

		setShowEdit(true);
	};

	const closeEdit = () => {

		setShowEdit(false);
		setName("");
		setDescription("");
		setPrice(0);

	};

	const addService = (e) => {

		e.preventDefault()

		fetch(`https://dandj-volleyball.herokuapp.com/services`, {
			method: 'POST',
			headers: {
				Authorization: `Bearer ${localStorage.getItem('token') }`,
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				name: name,
				description: description,
				price: price
			})
		})
		.then(res => res.json())
		.then(data => {

			if (data) {

				fetchData();

				Swal.fire({
					title: "Success",
					icon: "success",
					text: "Services successfully added."					
				})

				setName("")
				setDescription("")
				setPrice(0)

				closeAdd();

			} else {

				fetchData();

				Swal.fire({
					title: "Something went wrong",
					icon: "error",
					text: "Please try again."
				})
			}
		})
	}

	const updateService = (e, serviceId) => {
		
		e.preventDefault();

		fetch(`https://dandj-volleyball.herokuapp.com/services/${ serviceId }`, {
			method: 'PUT',
			headers: {
				'Content-Type': 'application/json',
				Authorization: `Bearer ${ localStorage.getItem('token') }`
			},
			body: JSON.stringify({
				name: name,
				description: description,
				price: price
			})
		})
		.then(res => res.json())
		.then(data => {
			console.log(data)

			if (data) {

				fetchData();

				Swal.fire({
					title: "Success",
					icon: "success",
					text: "Service successfully updated."
				});

				closeEdit();

			} else {

				fetchData();

				Swal.fire({
					title: "Something went wrong",
					icon: "error",
					text: "Please try again."
				});

			}

		})
	}

		const archiveToggle = (serviceId, isActive) => {

			console.log(!isActive);

			fetch(`https://dandj-volleyball.herokuapp.com/services/archieve/${ serviceId }`, {
				method: 'PUT',
				headers: {
					"Content-Type": "application/json",
					Authorization: `Bearer ${ localStorage.getItem('token') }`
				},
				body: JSON.stringify({
					isActive: !isActive
				})
			})
			.then(res => res.json())
			.then(data => {

				if (data) {
					console.log(data)

					fetchData();

					Swal.fire({
						title: "Success",
						icon: "success",
						text: "Service successfully archived."
					});

				} else {

					fetchData();

					Swal.fire({
						title: "Something went wrong",
						icon: "error",
						text: "Please try again."
					});

				}
			})
		}

		const unarchiveToggle = (serviceId, isActive) => {

			console.log(!isActive);

			fetch(`https://dandj-volleyball.herokuapp.com/services/unarchieve`, {
				method: 'PUT',
				headers: {
					"Content-Type": "application/json",
					Authorization: `Bearer ${ localStorage.getItem('token') }`
				}
			})
			.then(res => res.json())
			.then(data => {

				if (data) {
					console.log(data)

					fetchData();

					Swal.fire({
						title: "Success",
						icon: "success",
						text: "Service successfully unarchived."
					});

				} else {

					fetchData();

					Swal.fire({
						title: "Something went wrong",
						icon: "error",
						text: "Please try again."
					});

				}
			})
		}

useEffect(() => {

		const servicesArr = serviceData.map(service => {

			return(

				<tr key={service._id}>
					<td>{service.name}</td>
					<td>{service.description}</td>
					<td>{service.price}</td>
					<td>

						{service.isActive
							? <span>Available</span>
							: <span>Unavailable</span>
						}
					</td>
					<td>
						<Button
							variant="primary"
							size="sm"
							onClick={() => openEdit(service._id)}
						>
							Update
						</Button>
						
						{service.isActive
							?
							<Button 
								variant="danger" 
								size="sm" 
								onClick={() => archiveToggle(service._id, service.isActive)}
							>
								Disable
							</Button>
							:
							<Button 
								variant="success"
								size="sm"
								onClick={() => unarchiveToggle(service._id, service.isActive)}
							>
								Enable
							</Button>
						}
					</td>
				</tr>

			)

		});

		setServices(servicesArr);

	}, [serviceData, fetchData]);

	return(
		<Fragment>

			<div className="text-center my-4">
				<h2>Admin Dashboard</h2>
				<div className="d-flex justify-content-center">
					<Button variant="primary" onClick={openAdd}>Add New Service</Button>			
				</div>			
			</div>

			<Table striped bordered hover responsive>
				<thead className="bg-dark text-white">
					<tr>
						<th>Name</th>
						<th>Description</th>
						<th>Price</th>
						<th>Availability</th>
						<th>Actions</th>
					</tr>					
				</thead>
				<tbody>
					{services}
				</tbody>
			</Table>

			<Modal show={showAdd} onHide={closeAdd}>
				<Form onSubmit={e => addService(e)}>
					<Modal.Header closeButton>
						<Modal.Title>Add Service</Modal.Title>
					</Modal.Header>
					<Modal.Body>	
						<Form.Group controlId="serviceName">
							<Form.Label>Name</Form.Label>
							<Form.Control type="text" value={name} onChange={e => setName(e.target.value)} required/>
						</Form.Group>
						<Form.Group controlId="serviceDescription">
							<Form.Label>Description</Form.Label>
							<Form.Control type="text" value={description}  onChange={e => setDescription(e.target.value)} required/>
						</Form.Group>
						<Form.Group controlId="servicePrice">
							<Form.Label>Price</Form.Label>
							<Form.Control type="number" value={price}  onChange={e => setPrice(e.target.value)} required/>
						</Form.Group>
					</Modal.Body>
					<Modal.Footer>
						<Button variant="secondary" onClick={closeAdd}>Close</Button>
						<Button variant="success" type="submit">Submit</Button>
					</Modal.Footer>
				</Form>
			</Modal>

			<Modal show={showEdit} onHide={closeEdit}>
				<Form onSubmit={e => updateService(e, serviceId)}>
					<Modal.Header closeButton>
						<Modal.Title>Edit Service</Modal.Title>
					</Modal.Header>
					<Modal.Body>	
						<Form.Group controlId="serviceName">
							<Form.Label>Name</Form.Label>
							<Form.Control type="text" value={name} onChange={e => setName(e.target.value)} required/>
						</Form.Group>
						<Form.Group controlId="serviceDescription">
							<Form.Label>Description</Form.Label>
							<Form.Control type="text" value={description}  onChange={e => setDescription(e.target.value)} required/>
						</Form.Group>
						<Form.Group controlId="servicePrice">
							<Form.Label>Price</Form.Label>
							<Form.Control type="number" value={price}  onChange={e => setPrice(e.target.value)} required/>
						</Form.Group>
					</Modal.Body>
					<Modal.Footer>
						<Button variant="secondary" onClick={closeEdit}>Close</Button>
						<Button variant="success" type="submit">Submit</Button>
					</Modal.Footer>
				</Form>
			</Modal>
			
		</Fragment>
	)
}
