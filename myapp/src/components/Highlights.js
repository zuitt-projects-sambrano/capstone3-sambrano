import React from 'react';

import { Row, Col, Card } from 'react-bootstrap';

export default function Highlights(){
	return(
		<Row className="mt-3 mb-3">

			<Col xs={12} md={4}>
				<Card className="cardHighlight p-3">
					<Card.Body>
						<Card.Title>
							<h2>Learn how to play volleyball, from zero to good</h2>
						</Card.Title>
						<Card.Text>
							You'll learn how to develop proper technique for all basic volleyball skills. All tutorials are very detailed, easy to understand and adaptable to various playing situations. Demonstrations from various angles, in slow motion, and with different speeds and adaptations will help you to replicate them easily.
						</Card.Text>
					</Card.Body>

				</Card>
			</Col>

			<Col xs={12} md={4}>
				<Card className="cardHighlight p-3">
					<Card.Body>
						<Card.Title>
							<h2>Study Now, Pay Later</h2>
						</Card.Title>
						<Card.Text>
                            Available for services valued between Php 1,000 to Php 5,000, Study Now Pay Later is a flexible, interest-free payment solution that allows you to get paid upfront, while students spread their course cost for up to 36 months. This means you enjoy increased cashflow while your students receive stress free funding.
						</Card.Text>
					</Card.Body>

				</Card>
			</Col>

			<Col xs={12} md={4}>
				<Card className="cardHighlight p-3">
					<Card.Body>
						<Card.Title>
							<h2>Be Part Of Our Growing Family</h2>
						</Card.Title>
						<Card.Text>
							Learn how to play volleyball in just 3 months and can be a professional player.
						</Card.Text>
					</Card.Body>

				</Card>
			</Col>


		</Row>

	)
}